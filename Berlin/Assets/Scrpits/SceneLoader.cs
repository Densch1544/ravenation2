using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    
    void Start()
    {
        
    }

    
    void Update()
    {
        
    }

    public void HaloLoader()
    {
        SceneManager.LoadScene("Halo");
    }

    public void DocksLoader()
    {
        SceneManager.LoadScene("Docks");
    }

    public void IrishLoader()
    {
        SceneManager.LoadScene("Irish");
    }

    public void IntroLoader()
    {
        SceneManager.LoadScene("Intro");
    }

    public void HubLoader()
    {
        SceneManager.LoadScene("Hub");
    }

    public void MenuLoader()
    {
        SceneManager.LoadScene("Menu");
    }

    public void CreditsLoader()
    {
        SceneManager.LoadScene("Credits");
    }

   

    public void QuitGame()
    {
        Application.Quit();
    }
}
