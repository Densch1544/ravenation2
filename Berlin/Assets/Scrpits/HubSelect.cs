using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HubSelect : MonoBehaviour
{
    public GameObject Halo;
    public GameObject Docks;
    public GameObject Irish;
    void Start()
    {
        Halo.SetActive(false);
        Docks.SetActive(false);
        Irish.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HaloSelect()
    {
        Halo.SetActive(true);
        Docks.SetActive(false);
        Irish.SetActive(false);
    }

    public void DocksSelect()
    {
        Halo.SetActive(false);
        Docks.SetActive(true);
        Irish.SetActive(false);
    }

    public void IrishSelect()
    {
        Halo.SetActive(false);
        Docks.SetActive(false);
        Irish.SetActive(true);
    }
}
